package com.adarsh.emailautomation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class App {

    // Replace sender@example.com with your "From" address.
    // This address must be verified.
    static final String FROM = "silverghost1983@hotmail.com";
    static final String FROMNAME = "Adarsh Atluri";
	
    // Replace recipient@example.com with a "To" address. If your account 
    // is still in the sandbox, this address must be verified.
    static final String TO = "adarsh.atluri@gmail.com";
    
    // Replace smtp_username with your Amazon SES SMTP user name.
    static final String SMTP_USERNAME = "AKIAJKLKCLYXBJCNNN4Q";
    
    // Replace smtp_password with your Amazon SES SMTP password.
    static final String SMTP_PASSWORD = "AsHDQ5/H2aEJMdR5hv8JKS7Qz6W/2c/qO2uOoT8kPPT2";
    
    // The name of the Configuration Set to use for this message.
    // If you comment out or remove this variable, you will also need to
    // comment out or remove the header below.
  //  static final String CONFIGSET = "ConfigSet";
    
    // Amazon SES SMTP host name. This example uses the US West (Oregon) region.
    // See https://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html#region-endpoints
    // for more information.
    static final String HOST = "email-smtp.us-west-2.amazonaws.com";
    
    // The port you will connect to on the Amazon SES SMTP endpoint. 
    static final int PORT = 587;
    
    static final String SUBJECT = "Introductory E-mail on Trademark Services in India, AtlasIP, India";
    
    static final String BODY = String.join(
    	    System.getProperty("line.separator"),
    	    "<!DOCTYPE html>"
    	    +"<html><head><style>"
    	    +"table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%; }"
    	    +"td{border: 1px solid #000000;text-align: left;padding: 8px;word-wrap:break-word;background-color: #f2f2f2;}"
    	    +"th{border: 1px solid #000000;text-align: left;padding: 8px;word-wrap:break-word;background-color: #f2f2f2;text-align:center;color:#007399;}"
    	    +"</style></head>"
    	    +"<body>"
    	    +"<p>Dear Sirs,</p>"
    	    +"<p>Hope this e-mail finds you very well</p>"
    	    +"<p>We take this opportunity to introduce our firm <b>AtlasIP</b> and wish to explore mutual opportunities with you and your good firm.</p>"
    	    +"<p>We are an India based law firm established with the primary objective to provide sophisticated and tailored intellectual property services in a timely and efficient manner. We specialise in IPR matters including Trademarks, Designs and Copyrights protection from filing up to registration; search and watch services; and litigation. We take care of each and every clients’ intellectual property needs with utmost care and attention. Our firm is principled to have an open, honest, transparent and a strong <b>no surprise at billing credo</b>.</p>"
    	    +"<p>With an effort to acquaint you with our very competitive fee structure, we enclose herewith in the table below our trademark Schedule of Charges for India. As you may see, we offer the most competitive cost compared to those offered by other local firms without sacrifice in the quality of work."
    	    +"  <b>Also, we now look forward to receiving your schedule of fees for our reference and future consideration</b>.</p>"
    	    +"<h3>Fees for Trademark Services (India)</h3>"
    	    +"<table>"
    	    +"  <tr>"
    	    +"    <th>Particulars</th>"
    	    +"    <th>Official Fee(USD)</th>"
    	    +"    <th>Professional Fee (USD)</th>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Conducting comprehensive trademark search in India per mark per class.</td>"
    	    +"    <td>None</td>"
    	    +"    <td>250</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Attending to registration of a single-class application, including preparing and filing application, scheduled status checks, reporting of any Trademark Office’s or third party’s objections, monitoring for publication, reporting publication, Watch on the TM journal weekly for identical/ confusingly similar trademarks.</td>"
    	    +"    <td>135</td>"
    	    +"    <td>300</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Providing comments/recommendations on the official report/provisional refusal, drafting and filing a relevant response to the examination report/provisional refusal.</td>"
    	    +"    <td>None</td>"
    	    +"    <td>400</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Attending one hearing regarding the examination report/provisional refusal.</td>"
    	    +"    <td>None</td>"
    	    +"    <td>300</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Obtaining registration certificate, proof reading and forwarding the same to you, reporting regarding the due date of renewal of a trademark.</td>"
    	    +"    <td>None</td>"
    	    +"    <td>100</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Renewal of a trademark every ten years.</td>"
    	    +"    <td>135</td>"
    	    +"    <td>200</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Renewal of a trademark with surcharge.</td>"
    	    +"    <td>202</td>"
    	    +"    <td>200</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Restoration and renewal of an expired mark.</td>"
    	    +"    <td>270</td>"
    	    +"    <td>250</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Drafting and filing opposition notice against an impugned mark</td>"
    	    +"    <td>32</td>"
    	    +"    <td>450</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Drafting and filing evidence affidavits in support of opposition notice</td>"
    	    +"    <td>None</td>"
    	    +"    <td>450</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Drafting and filing evidence affidavit in reply</td>"
    	    +"    <td>None</td>"
    	    +"    <td>450</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Attending one hearing</td>"
    	    +"    <td>None</td>"
    	    +"    <td>450</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Drafting and filing a Counterstatement</td>"
    	    +"    <td>25</td>"
    	    +"    <td>500</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Drafting and filing evidence affidavits in support of application</td>"
    	    +"    <td>None</td>"
    	    +"    <td>500</td>"
    	    +"  </tr>"
    	    +"  <tr>"
    	    +"    <td>Attending one hearing</td>"
    	    +"    <td>None</td>"
    	    +"    <td>500</td>"
    	    +"</table>"
    	    +"<p><b>** The above-mentioned fees are inclusive of Official Fees, Professional Fees and Disbursements per mark per class. No extra/hidden charges are applied.</b></p>"
    	    +"<p>We would be pleased to assist you with IP matters in India and look forward to cooperation with you and your firm. Should you require additional information regarding other IP services, kindly do not hesitate to write to us at info@atlasiplegal.com.</p>"
    	    +"<p>Looking forward to the first opportunity of working with you.</p>"
    	    +"<p>Kind Regards,<br> Ann George</p>"
    	    +"<p>Business Associate</p>"
    	    +"<img src=https://s3-us-west-2.amazonaws.com/aatluri-emailautomation/atlasiplegallogo.png alt=Atlas Ip Legal Logo width=120 height=60>"
    	    +"</body></html>"

    	);

    public static void main(String[] args) throws Exception {

    	String csvFile = "/Users/adarshatluri/Downloads/LawFirms.csv";
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] emailParams = line.split(cvsSplitBy);
                String toEmail = emailParams[2];
                System.out.println("Sending email to " + toEmail);
                sendEmail(toEmail);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    	
    }
    
    public static void sendEmail(String toEmail) throws UnsupportedEncodingException, MessagingException
    {
    	
    	// Create a Properties object to contain connection configuration information.
    	Properties props = System.getProperties();
    	props.put("mail.transport.protocol", "smtp");
    	props.put("mail.smtp.port", PORT); 
    	props.put("mail.smtp.starttls.enable", "true");
    	props.put("mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified properties. 
    	Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM,FROMNAME));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
        msg.setSubject(SUBJECT);
        msg.setContent(BODY,"text/html");
        
        // Add a configuration set header. Comment or delete the 
        // next line if you are not using a configuration set
       // msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
            
        // Create a transport.
        Transport transport = session.getTransport();
                    
        // Send the message.
        try
        {
            System.out.println("Sending...");
            
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
        	
            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }
        catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
        finally
        {
            // Close and terminate the connection.
            transport.close();
        }
    }
}